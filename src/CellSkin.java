
import java.util.Random;
import javafx.scene.Node;
import javafx.scene.control.Control;
import javafx.scene.control.Skin;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class CellSkin extends Control implements Skin, Constantes {

    private final Cell cell;
    Random rnd = new Random();

    public CellSkin(Cell cell) {
        this.cell = cell;
    }

    @Override
    public Cell getSkinnable() {
        return cell;
    }

    @Override
    public Node getNode() {
        Pane root = new Pane();
        root.setPrefSize(DEEP, DEEP);
        Text t;
        ImageView ground = new ImageView(new Image("images/GROUND.png"));
        ImageView sidewalk = new ImageView(new Image("images/parquet.png"));
        ImageView player = new ImageView(new Image("images/PLAYER.png"));
        ImageView building1 = new ImageView(new Image("images/apartment.png"));
        ImageView building2 = new ImageView(new Image("images/city.png"));
        ImageView building3 = new ImageView(new Image("images/home.png"));
        ImageView building4 = new ImageView(new Image("images/home-1.png"));
        ImageView way_h = new ImageView(new Image("images/WAY_H.png"));
        ImageView way_v = new ImageView(new Image("images/WAY_V.png"));
        ImageView meta = new ImageView(new Image("images/META2.png"));
        ImageView car_down = new ImageView(new Image("images/CAR_DOWN.png"));
        ImageView car_up = new ImageView(new Image("images/CAR_UP.png"));
        ImageView car_left = new ImageView(new Image("images/CAR_LEFT.png"));
        ImageView car_right = new ImageView(new Image("images/CAR_RIGHT.png"));
        ImageView walker = new ImageView(new Image("images/WALKER1.png"));
        ImageView walker2 = new ImageView(new Image("images/WALKER2.png"));
        ImageView tree = new ImageView(new Image("images/TREE.png"));
        ImageView zebra_v = new ImageView(new Image("images/ZEBRA_V.png"));
        ImageView zebra_h = new ImageView(new Image("images/ZEBRA_H.png"));
        ImageView bus = new ImageView(new Image("images/BUS.png"));
        ImageView bus_stop = new ImageView(new Image("images/BUS_STOP.png"));
        ImageView passenger = new ImageView(new Image("images/PASSENGER.png"));
        ImageView letter = new ImageView(new Image("images/LETTER.png"));
        ImageView thief = new ImageView(new Image("images/THIEF.png"));

        switch (cell.ground) {
            case NORMAL:
                root.getChildren().add(ground);
                break;
            case WAY_H:
                root.getChildren().add(ground);
                root.getChildren().add(way_h);
                break;
            case WAY_V:
                root.getChildren().add(ground);
                root.getChildren().add(way_v);
                break;
            case SIDEWALK:
                root.getChildren().add(sidewalk);
                break;
            case ZEBRA_V:
                root.getChildren().add(ground);
                root.getChildren().add(zebra_v);
                break;
            case ZEBRA_H:
                root.getChildren().add(ground);
                root.getChildren().add(zebra_h);
                break;
        }

        // CONTENT
        switch (cell.content) {
            case META:
                root.getChildren().add(meta);
                break;
            case BUILDING:
                switch ((int) ((new Random()).nextDouble() * 4)) {
                    case 0:
                        root.getChildren().add(building3);
                        break;
                    case 1:
                        root.getChildren().add(building2);
                        break;
                    case 2:
                        root.getChildren().add(building1);
                        break;
                    case 3:
                        root.getChildren().add(building4);
                        break;
                }
                break;
            case CAR_DOWN:
                root.getChildren().add(car_down);
                break;
            case CAR_UP:
                root.getChildren().add(car_up);
                break;
            case CAR_LEFT:
                root.getChildren().add(car_left);
                break;
            case CAR_RIGHT:
                root.getChildren().add(car_right);
                break;
            case TEXT:
                t = new Text(cell.text);
                t.setFont(Font.font("Verdana", 15));
                t.setFill(Color.YELLOW);
                root.getChildren().add(t);
                break;
            case PLAYER:
                root.getChildren().add(player);
                break;
            case WALKER:
                switch ((int) (rnd.nextDouble() * 2)) {
                    case 0:
                        root.getChildren().add(walker);
                        break;
                    case 1:
                        root.getChildren().add(walker2);
                        break;
                    default:
                        root.getChildren().add(walker);
                        break;
                }
                break;
            case TREE:
                root.getChildren().add(tree);
                break;
            case BUS:
                root.getChildren().add(bus);
                break;
            case BUS_STOP:
                root.getChildren().add(bus_stop);
                break;
            case PASSENGER:
                root.getChildren().add(passenger);
                break;
            case LETTER:
                root.getChildren().add(letter);
                break;
            case THIEF:
                root.getChildren().add(thief);
                break;
        }

        return root;
    }

    @Override
    public void dispose() {

    }

}
