
import java.util.ArrayList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class BusquedaRutaPlayerHuida extends TimerTask {

    Cell[][] grid;
    Queue<Estado> colaEstados;
    ArrayList<Estado> historial;
    ArrayList<Estado> destinos;
    ArrayList<Estado.Operator> pasos;
    Estado inicial;
    Estado objetivo;
    Estado temp;
    Player player;
    boolean exito;
    boolean parar;

    public BusquedaRutaPlayerHuida(Cell[][] grid, Player player) {
        this.grid = grid;
        colaEstados = new PriorityQueue<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        destinos = new ArrayList<>();

        //digo cual es el estado inicial y el final
        exito = false;
        this.player = player;
        parar = false;
    }

    public boolean buscar(Estado inicial, Estado objetivo) {

        // //    System.out.println("Estado inicial" + inicial.toString());
        //    System.out.println("Estado objetivo" + objetivo.toString());
//        
        inicial.prioridad = calcularDistancia(inicial.x, inicial.y, World.getXThief(), World.getYThief());
        System.out.println(calcularDistancia(inicial.x, inicial.y, World.getXThief(), World.getYThief()));
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo = objetivo;
        exito = false;

        if (inicial.equals(objetivo)) {
            exito = true;
        }

        while (!colaEstados.isEmpty() && !exito) {
            temp = colaEstados.poll();
            buscarArriba(temp);
            buscarAbajo(temp);
            buscarIzquierda(temp);
            buscarDerecha(temp);
        }

        if (exito) {
            //    System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        } else {
            //    System.out.println("La ruta no pudo calcularse");
            return false;
        }

    }

    public double calcularDistancia(int x1, int y1, int x2, int y2) {
        double valor;
        double parte1 = Math.pow(Math.abs(x2 - x1), 2);
        double parte2 = Math.pow(Math.abs(y2 - y1), 2);
        parte1 += parte2;
        valor = Math.sqrt(parte1);
        return valor;
    }

    private void buscarArriba(Estado e) {
        if (e.y > 0) {
            if (grid[e.x][e.y - 1].isWalkable()) {
                Estado arriba = new Estado(e.x, e.y - 1, Estado.Operator.UP, e);
                arriba.prioridad = calcularDistancia(arriba.x, arriba.y, World.getXThief(), World.getYThief());
                if (!historial.contains(arriba)) {
                    colaEstados.add(arriba);
                    historial.add(arriba);
                    if (arriba.equals(objetivo)) {
                        objetivo = arriba;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarAbajo(Estado e) {
        if (e.y < 19) {
            if (grid[e.x][e.y + 1].isWalkable()) {
                Estado abajo = new Estado(e.x, e.y + 1, Estado.Operator.DOWN, e);
                abajo.prioridad = calcularDistancia(abajo.x, abajo.y, World.getXThief(), World.getYThief());
                if (!historial.contains(abajo)) {
                    colaEstados.add(abajo);
                    historial.add(abajo);
                    if (abajo.equals(objetivo)) {
                        objetivo = abajo;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarIzquierda(Estado e) {
        if (e.x > 0) {
            if (grid[e.x - 1][e.y].isWalkable()) {
                Estado izquierda = new Estado(e.x - 1, e.y, Estado.Operator.LEFT, e);
                izquierda.prioridad = calcularDistancia(izquierda.x, izquierda.y, World.getXThief(), World.getYThief());
                if (!historial.contains(izquierda)) {
                    colaEstados.add(izquierda);
                    historial.add(izquierda);
                    if (izquierda.equals(objetivo)) {
                        objetivo = izquierda;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarDerecha(Estado e) {
        if (e.x < 19) {
            if (grid[e.x + 1][e.y].isWalkable()) {
                Estado derecha = new Estado(e.x + 1, e.y, Estado.Operator.RIGHT, e);
                derecha.prioridad = calcularDistancia(derecha.x, derecha.y, World.getXThief(), World.getYThief());
                if (!historial.contains(derecha)) {
                    colaEstados.add(derecha);
                    historial.add(derecha);
                    if (derecha.equals(objetivo)) {
                        objetivo = derecha;
                        exito = true;
                    }
                }
            }
        }
    }

    public void calcularRuta() {
        Estado predecesor = objetivo;
        do {
            pasos.add(0, predecesor.operator);
            predecesor = predecesor.anterior;
        } while (predecesor != null);
    }

    @Override
    public synchronized void run() {
        if (!parar) {

            colaEstados.clear();
            historial.clear();
            pasos.clear();

            Estado subinicial, subobjetivo;
            boolean resultado;

            do {

                subinicial = new Estado(player.x, player.y, Estado.Operator.NOTHING, null);

                subobjetivo = destinos.get(0);
                resultado = this.buscar(subinicial, subobjetivo);

                if (subinicial.equals(subobjetivo)) {
                    destinos.remove(subobjetivo);
                } else if (!resultado) {
                    colaEstados.clear();
                    historial.clear();
                    pasos.clear();
                    destinos.remove(subobjetivo);
                }

                if (destinos.isEmpty()) {
                    // //    System.out.println("Se acabo a donde ir");
                    this.cancel();
                }

            } while (!resultado && !destinos.isEmpty());

            if (pasos.size() > 1) {
                switch (pasos.get(1)) {
                    case DOWN:
                        Platform.runLater(() -> {
                            player.moveToDown();
                        });
                        break;
                    case UP:
                        Platform.runLater(() -> {
                            player.moveToUp();
                        });
                        break;
                    case RIGHT:
                        Platform.runLater(() -> {
                            player.moveToRight();
                        });
                        break;
                    case LEFT:
                        Platform.runLater(() -> {
                            player.moveToLeft();
                        });
                        break;
                }
            }
        }
    }

}
