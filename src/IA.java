/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author pdelgado
 */
public class IA extends Application implements Constantes {

    @Override
    public void start(Stage primaryStage) {

        World world = new World();

        Scene scene = world.getScene();

        primaryStage.setTitle("TAREA IA");
        primaryStage.setScene(scene);

        primaryStage.setResizable(false);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
