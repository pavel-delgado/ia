
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class Walker extends TimerTask {

    Cell[][] grid;
    int y;
    int x;

    Direction dir = Direction.RIGHT;

    @Override
    public void run() {
        walkerMove();
    }

    public Walker(Cell[][] grid, int x, int y) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        grid[x][y].setContent(Cell.CellContent.WALKER);

    }

    public static enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public void walkerMove() {
        Platform.runLater(() -> {
            switch (dir) {
                case RIGHT:
                    if (checkMove(grid[x + 1][y])) {
                        grid[x + 1][y].setContent(Cell.CellContent.WALKER);
                        grid[x][y].clearContent();
                        x++;
                    } else {
                        dir = Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case LEFT:
                    if (checkMove(grid[x - 1][y])) {
                        grid[x - 1][y].setContent(Cell.CellContent.WALKER);
                        grid[x][y].clearContent();
                        x--;
                    } else {
                        dir = Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case UP:
                    if (checkMove(grid[x][y + 1])) {
                        grid[x][y + 1].setContent(Cell.CellContent.WALKER);
                        grid[x][y].clearContent();
                        y++;
                    } else {
                        dir = Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case DOWN:
                    if (checkMove(grid[x][y - 1])) {
                        grid[x][y - 1].setContent(Cell.CellContent.WALKER);
                        grid[x][y].clearContent();
                        y--;
                    } else {
                        dir = Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;

            }
        });
    }

    public boolean checkMove(Cell cell) {
        boolean ground = false;
        boolean content = false;

        switch (cell.ground) {
            case SIDEWALK:
                ground = true;
                break;
        }

        switch (cell.content) {
            case NULL:
                content = true;
                break;
            case LETTER:
                content = true;
                break;
        }

        return ground && content;

    }
}
