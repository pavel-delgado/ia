/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author NICK
 */
public class Estado implements Comparable {

    int x;
    int y;
    Operator operator;
    Estado anterior;
    public double prioridad;

    public static enum Operator {
        LEFT,
        RIGHT,
        UP,
        DOWN,
        NOTHING
    }

    Estado(int x, int y, Operator oper, Estado anterior) {
        this.x = x;
        this.y = y;
        this.operator = oper;
        this.anterior = anterior;
    }

    @Override
    public boolean equals(Object x) {
        Estado e = (Estado) x;
        return this.x == e.x && this.y == e.y;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

    @Override
    public int compareTo(Object o) {
        Estado e = (Estado) o;
//        System.out.println(e.prioridad);
//        System.out.println(this.prioridad);
        if (this.prioridad == e.prioridad) {
            return 0;
        } else if (this.prioridad > e.prioridad) {
            return 1;
        } else {
            return -1;
        }
    }
}
