
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.FlowPane;

public class World implements Constantes {

    Cell grid[][];
    FlowPane root;
    Player player;
    Timer playerTimer = new Timer();
    static Thief thief;
    Random rnd = new Random();
    ArrayList<Block> blockList = new ArrayList<>();

    World() {
        grid = new Cell[WIDTH][HEIGHT];
        root = new FlowPane();
    }

    public static int getXThief() {
        return 1;
    }

    public static int getYThief() {
        return 1;
    }

    public Parent createWorld() {
        for (int i = 0; i < HEIGHT; i++) {
            for (int j = 0; j < WIDTH; j++) {
                grid[j][i] = new Cell(Cell.CellGround.NORMAL, Cell.CellContent.NULL);
                root.getChildren().add(grid[j][i]);
            }
        }

        createBlocks();
        createPlayer();
        createIA();
        updateLettersGUI();
//        createZebra();
//        createBus();
        createTrafic();

//        createThief();
        return root;
    }

    public void createTrafic() {
        int ranx, rany;
        int trafico = T;
        Timer timer = new Timer();

        while (trafico > 0) {
            ranx = (int) ((new Random()).nextDouble() * WIDTH);
            rany = (int) ((new Random()).nextDouble() * HEIGHT);

            if (grid[ranx][rany].ground == Cell.CellGround.WAY_H || grid[ranx][rany].ground == Cell.CellGround.WAY_V) {
                timer.scheduleAtFixedRate(new Car(grid, ranx, rany), 5000, 1000);
                trafico--;
            }

        }

    }

    public Scene getScene() {
        return new Scene(createWorld(), DEEP * WIDTH, DEEP * HEIGHT);
    }

    public void createPlayer() {
        player = new Player(grid, 0, 2, this);
        createPlayerListeners();
    }

    public void createBus() {
        Bus bus = new Bus(grid, 0, 7, N_PASAJEROS);
        Thread busThread = new Thread(bus);
        busThread.setDaemon(true);
        busThread.start();

        grid[2][6].setContent(Cell.CellContent.BUS_STOP);
        grid[6][6].setContent(Cell.CellContent.BUS_STOP);
        grid[10][8].setContent(Cell.CellContent.BUS_STOP);
        grid[16][6].setContent(Cell.CellContent.BUS_STOP);
    }

    private void createPlayerListeners() {
        root.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case UP:
                    player.moveToUp();
                    break;
                case DOWN:
                    player.moveToDown();
                    break;
                case LEFT:
                    player.moveToLeft();
                    break;
                case RIGHT:
                    player.moveToRight();
                    break;
            }
        });

    }

    public static boolean checkMarginLeft(int x) {
        return ((x - 1) > -1);
    }

    public static boolean checkMarginRight(int x) {
        return ((x + 1) < WIDTH);
    }

    public static boolean checkMarginTop(int y) {
        return ((y - 1) > -1);
    }

    public static boolean checkMarginBottom(int y) {
        return ((y + 1) < HEIGHT);
    }

    private void createIA() {

        playerTimer.scheduleAtFixedRate(player.ia, 10000, 300);
    }

    private void createThief() {
        thief = new Thief(grid, 19, 19, player, playerTimer);
        Timer lanzadorTareas = new Timer();
        lanzadorTareas.scheduleAtFixedRate(thief.ia, 10000, 1200);
    }

    private void createBlocks() {

        blockList.add(new Block(grid, 3, 4, 10));
        blockList.add(new Block(grid, 9, 4, 100));
        blockList.add(new Block(grid, 15, 4, 1000));

        blockList.add(new Block(grid, 3, 10, 1000));
        blockList.add(new Block(grid, 9, 10, 10));
        blockList.add(new Block(grid, 15, 10, 100));

        blockList.add(new Block(grid, 3, 16, 100));
        blockList.add(new Block(grid, 9, 16, 1000));
        blockList.add(new Block(grid, 15, 16, 10));
    }

    public void updateLettersGUI() {
        System.out.println("DESTINOS " + player.ia.destinos.size());
        for (int i = 0; i < LETTERS.size(); i++) {
            if (i < HEIGHT) {
                grid[20][i + 5].setText("Carta Nº" + (i + 1) + " " + LETTERS.get(i).toString());
            }
        }
        if (player.ia.objetivo != null) {

        }

    }
}
