
import java.util.Random;
import java.util.Timer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class Block implements Constantes {

    Cell[][] grid;
    int y;
    int x;
    int walkers;

    public Block(Cell[][] grid, int x, int y, int walkers) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        this.walkers = walkers;
        // TEXT
        grid[x][y].setText(this.walkers + "");
        // HOUSES
        grid[x][y + 1].setContent(Cell.CellContent.BUILDING);
        grid[x][y - 1].setContent(Cell.CellContent.BUILDING);
        grid[x + 1][y].setContent(Cell.CellContent.BUILDING);
        grid[x + 1][y + 1].setContent(Cell.CellContent.BUILDING);
        grid[x + 1][y - 1].setContent(Cell.CellContent.BUILDING);
        PORTALS.add(new Portal(grid, x - 1, y));
        grid[x - 1][y + 1].setContent(Cell.CellContent.BUILDING);
        grid[x - 1][y - 1].setContent(Cell.CellContent.BUILDING);
        // SIDEWALKS + WALKERS
        grid[x - 2][y].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 2][y].setWalkers(walkers);

        grid[x - 2][y - 1].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 2][y - 1].setWalkers(walkers);

        grid[x - 2][y - 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 2][y - 2].setWalkers(walkers);

        grid[x - 1][y - 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 1][y - 2].setWalkers(walkers);

        grid[x + 1][y - 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 1][y - 2].setWalkers(walkers);

        grid[x - 2][y + 1].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 2][y + 1].setWalkers(walkers);

        grid[x - 2][y + 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 2][y + 2].setWalkers(walkers);

        grid[x + 2][y].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 2][y].setWalkers(walkers);

        grid[x + 2][y - 1].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 2][y - 1].setWalkers(walkers);

        grid[x + 2][y - 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 2][y - 2].setWalkers(walkers);

        grid[x + 2][y + 1].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 2][y + 1].setWalkers(walkers);

        grid[x + 2][y + 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 2][y + 2].setWalkers(walkers);

        grid[x + 1][y + 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x + 1][y + 2].setWalkers(walkers);

        grid[x - 1][y + 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x - 1][y + 2].setWalkers(walkers);

        grid[x][y + 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x][y + 2].setWalkers(walkers);

        grid[x][y - 2].setGround(Cell.CellGround.SIDEWALK);
        grid[x][y - 2].setWalkers(walkers);

        // WAYS + ZEBRAS
        grid[x + 3][y - 2].setGround(Cell.CellGround.ZEBRA_H);
        grid[x + 3][y - 1].setGround(Cell.CellGround.WAY_V);
        grid[x + 3][y].setGround(Cell.CellGround.WAY_V);
        grid[x + 3][y + 1].setGround(Cell.CellGround.WAY_V);
        grid[x + 3][y + 2].setGround(Cell.CellGround.ZEBRA_H);

        grid[x - 3][y - 2].setGround(Cell.CellGround.ZEBRA_H);
        grid[x - 3][y - 1].setGround(Cell.CellGround.WAY_V);
        grid[x - 3][y].setGround(Cell.CellGround.WAY_V);
        grid[x - 3][y + 1].setGround(Cell.CellGround.WAY_V);
        grid[x - 3][y + 2].setGround(Cell.CellGround.ZEBRA_H);

        grid[x - 3][y - 3].setGround(Cell.CellGround.WAY_H);
        grid[x - 2][y - 3].setGround(Cell.CellGround.ZEBRA_V);
        grid[x - 1][y - 3].setGround(Cell.CellGround.WAY_H);
        grid[x][y - 3].setGround(Cell.CellGround.WAY_H);
        grid[x + 1][y - 3].setGround(Cell.CellGround.WAY_H);
        grid[x + 2][y - 3].setGround(Cell.CellGround.ZEBRA_V);
        grid[x + 3][y - 3].setGround(Cell.CellGround.WAY_H);

        grid[x - 3][y + 3].setGround(Cell.CellGround.WAY_H);
        grid[x - 2][y + 3].setGround(Cell.CellGround.ZEBRA_V);
        grid[x - 1][y + 3].setGround(Cell.CellGround.WAY_H);
        grid[x][y + 3].setGround(Cell.CellGround.WAY_H);
        grid[x + 1][y + 3].setGround(Cell.CellGround.WAY_H);
        grid[x + 2][y + 3].setGround(Cell.CellGround.ZEBRA_V);
        grid[x + 3][y + 3].setGround(Cell.CellGround.WAY_H);

        // WALKERS
        int ranx, rany;
        int population = (int) ((new Random()).nextDouble() * P);
        Timer timer = new Timer();

        while (population > 0) {
            ranx = (int) ((new Random()).nextDouble() * 3);
            rany = (int) ((new Random()).nextDouble() * 3);

            if (grid[x + ranx][y + rany].ground == Cell.CellGround.SIDEWALK) {
                timer.scheduleAtFixedRate(new Walker(grid, x + ranx, y + rany), 5000, 1000);
                population--;
                continue;
            }
            if (grid[x - ranx][y - rany].ground == Cell.CellGround.SIDEWALK) {
                timer.scheduleAtFixedRate(new Walker(grid, x - ranx, y - rany), 5000, 1000);
                population--;
                continue;
            }

            if (grid[x + ranx][y - rany].ground == Cell.CellGround.SIDEWALK) {
                timer.scheduleAtFixedRate(new Walker(grid, x + ranx, y - rany), 5000, 1000);
                population--;
                continue;
            }
            if (grid[x - ranx][y + rany].ground == Cell.CellGround.SIDEWALK) {
                timer.scheduleAtFixedRate(new Walker(grid, x - ranx, y + rany), 5000, 1000);
                population--;
            }
        }

    }

}
