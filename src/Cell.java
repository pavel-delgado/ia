
import javafx.scene.control.Control;
import javafx.scene.control.Skinnable;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class Cell extends Control implements Skinnable, Constantes {

    CellContent content = null;
    CellGround ground = CellGround.NORMAL;
    String text;
    int walkers = 0;

    public static enum CellContent {
        NULL,
        BUILDING,
        META,
        TEXT,
        CAR_UP,
        CAR_DOWN,
        CAR_LEFT,
        CAR_RIGHT,
        PLAYER,
        WALKER,
        TREE,
        BUS,
        BUS_STOP,
        PASSENGER,
        LETTER,
        THIEF
    }

    public static enum CellGround {
        NORMAL,
        SIDEWALK,
        WAY_H,
        WAY_V,
        ZEBRA_V,
        ZEBRA_H

    }

    public boolean isWalkable() {
        boolean xground;
        boolean xcontent;

        switch (this.ground) {
            case WAY_H:
                xground = !SEGURO;
                break;
            case WAY_V:
                xground = !SEGURO;
                break;
            default:
                xground = true;
                break;
        }

        switch (this.content) {
            case NULL:
                xcontent = true;
                break;
            case META:
                xcontent = true;
                break;
            default:
                xcontent = false;
                break;
        }

        return xground && xcontent;

    }

    public Cell(CellGround ground, CellContent content) {
        this.content = content;
        this.ground = ground;
        setSkin(new CellSkin(this));
        setMaxHeight(Double.MAX_VALUE);
        setMaxWidth(Double.MAX_VALUE);
    }

    public void setContent(CellContent content) {
        this.content = content;
        setSkin(null);
        setSkin(new CellSkin(this));
    }

    public void setGround(CellGround ground) {
        this.ground = ground;
        setSkin(null);
        setSkin(new CellSkin(this));
    }

    public void setWalkers(int walkers) {
        this.walkers = walkers;
    }

    public int getWalkers() {
        return walkers;
    }

    public void setText(String text) {
        this.text = text;
        this.setContent(CellContent.TEXT);
        setSkin(null);
        setSkin(new CellSkin(this));
    }

    public CellContent getContent() {
        return this.content;
    }

    public void clearContent() {
        this.content = CellContent.NULL;
        setSkin(null);
        setSkin(new CellSkin(this));
    }
}
