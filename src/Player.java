

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class Player implements Constantes {

    Cell[][] grid;
    int y;
    int x;
    BusquedaRutaPlayerInformadaPeaton ia;
    private final World world;

    public Player(Cell[][] grid, int x, int y, World world) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        this.world = world;

        grid[x][y].setContent(Cell.CellContent.PLAYER);

        ia = new BusquedaRutaPlayerInformadaPeaton(grid, this);

        // INPUT DE CARTAS
        LETTERS.add(new Letter(0));
        LETTERS.add(new Letter(3));
        LETTERS.add(new Letter(6));
        LETTERS.add(new Letter(5));
        LETTERS.add(new Letter(4));
        LETTERS.add(new Letter(4));
        LETTERS.add(new Letter(1));
        LETTERS.add(new Letter(2));

        // CARGA DE DESTINOS
        for (int i = 0; i < LETTERS.size(); i++) {

            if (!ia.destinos.contains(new Estado(PORTALS.get(LETTERS.get(i).portal).x, PORTALS.get(LETTERS.get(i).portal).y, Estado.Operator.NOTHING, null))) {
                ia.destinos.add(new Estado(PORTALS.get(LETTERS.get(i).portal).x, PORTALS.get(LETTERS.get(i).portal).y, Estado.Operator.NOTHING, null));
            }
        }

    }

    public void moveToLeft() {
        if (World.checkMarginLeft(x)) {
            if (checkMove(grid[x - 1][y])) {
                grid[x - 1][y].setContent(Cell.CellContent.PLAYER);
                grid[x][y].clearContent();
                x--;
            }
        }
    }

    public void moveToRight() {
        if (World.checkMarginRight(x)) {
            if (checkMove(grid[x + 1][y])) {
                grid[x + 1][y].setContent(Cell.CellContent.PLAYER);
                grid[x][y].clearContent();
                x++;
            }
        }
    }

    public void moveToDown() {
        if (World.checkMarginBottom(y)) {
            if (checkMove(grid[x][y + 1])) {
                grid[x][y + 1].setContent(Cell.CellContent.PLAYER);
                grid[x][y].clearContent();
                y++;
            }
        }

    }

    public void moveToUp() {
        if (World.checkMarginTop(y)) {
            if (checkMove(grid[x][y - 1])) {
                grid[x][y - 1].setContent(Cell.CellContent.PLAYER);
                grid[x][y].clearContent();
                y--;
            }
        }
    }

    public boolean checkMove(Cell cell) {
        boolean ground;
        boolean content;

        switch (cell.ground) {
            case WAY_H:
                ground = !SEGURO;
                break;
            case WAY_V:
                ground = !SEGURO;
                break;
            default:
                ground = true;
                break;
        }

        switch (cell.content) {
            case NULL:
                content = true;
                grid[TEXT_X][TEXT_Y - 1].setText("");
                break;
            case META:
                grid[TEXT_X][TEXT_Y - 1].setText("CARTERO EN PORTAL!!");
                for (int i = 0; i < LETTERS.size(); i++) {
                    if (PORTALS.get(LETTERS.get(i).portal).x == (this.x + 1) && PORTALS.get(LETTERS.get(i).portal).y == this.y) {
                        LETTERS.get(i).setDelivered(true);
                    }
                }
                world.updateLettersGUI();
                content = true;
                break;
            default:
                content = false;
                break;
        }

        return ground && content;
    }
}
