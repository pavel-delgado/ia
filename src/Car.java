
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class Car extends TimerTask {

    Cell[][] grid;
    int y;
    int x;

    Direction dir = Direction.RIGHT;

    @Override
    public void run() {
        autoMove();
    }

    public static enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public Car(Cell[][] grid, int x, int y) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        grid[x][y].setContent(Cell.CellContent.CAR_RIGHT);

    }

    public void autoMove() {
        Platform.runLater(() -> {
            switch (dir) {
                case RIGHT:
                    if (World.checkMarginRight(x)) {
                        if (checkMove(grid[x + 1][y])) {
                            grid[x + 1][y].setContent(Cell.CellContent.CAR_RIGHT);
                            grid[x][y].clearContent();
                            x++;
                        } else {
                            dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                        }
                    } else {
                        dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case LEFT:
                    if (World.checkMarginLeft(x)) {
                        if (checkMove(grid[x - 1][y])) {
                            grid[x - 1][y].setContent(Cell.CellContent.CAR_LEFT);
                            grid[x][y].clearContent();
                            x--;
                        } else {
                            dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                        }
                    } else {
                        dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case UP:
                    if (World.checkMarginTop(y)) {
                        if (checkMove(grid[x][y - 1])) {
                            grid[x][y - 1].setContent(Cell.CellContent.CAR_UP);
                            grid[x][y].clearContent();
                            y--;
                        } else {
                            dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                        }
                    } else {
                        dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
                case DOWN:
                    if (World.checkMarginBottom(y)) {
                        if (checkMove(grid[x][y + 1])) {
                            grid[x][y + 1].setContent(Cell.CellContent.CAR_DOWN);
                            grid[x][y].clearContent();
                            y++;
                        } else {
                            dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                        }
                    } else {
                        dir = Car.Direction.values()[(int) ((new Random()).nextDouble() * 4)];
                    }
                    break;
            }

        });

    }

    public boolean checkMove(Cell cell) {
        boolean ground = false;
        boolean content = false;

        switch (cell.ground) {
            case WAY_H:
                ground = true;
                break;
            case WAY_V:
                ground = true;
                break;
            case ZEBRA_V:
                ground = true;
                break;
            case ZEBRA_H:
                ground = true;
                break;
        }

        switch (cell.content) {
            case NULL:
                content = true;
                break;
            case LETTER:
                content = true;
                break;
        }

        return ground && content;

    }

}
