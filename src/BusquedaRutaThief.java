
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class BusquedaRutaThief extends TimerTask implements Runnable, Constantes {

    Cell[][] grid;
    ArrayList<Estado> colaEstados;
    ArrayList<Estado> historial;
    ArrayList<Estado> destinos;
    ArrayList<Estado.Operator> pasos;
    int indice_pasos;
    Estado inicial;
    Estado objetivo;
    Estado temp;
    Thief thief;
    boolean exito;
    boolean parar;
    Player player;
    Timer playerTimer;

    public BusquedaRutaThief(Cell[][] grid, Thief thief, Player player, Timer playerTimer) {
        this.grid = grid;
        colaEstados = new ArrayList<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        destinos = new ArrayList<>();
        indice_pasos = 0;
        //digo cual es el estado inicial y el final
        exito = false;
        this.thief = thief;
        this.player = player;
        parar = false;
        this.playerTimer = playerTimer;
    }

    public boolean buscar(Estado inicial, Estado objetivo) {

        System.out.println("Estado inicial" + inicial.toString());
        System.out.println("Estado objetivo" + objetivo.toString());
        indice_pasos = 0;
        colaEstados.add(inicial);
        historial.add(inicial);
        this.objetivo = objetivo;
        exito = false;

        if (inicial.equals(objetivo)) {
            exito = true;
        }

        while (!colaEstados.isEmpty() && !exito) {
            temp = colaEstados.get(0);
            colaEstados.remove(0);
            buscarArriba(temp);
            buscarAbajo(temp);
            buscarIzquierda(temp);
            buscarDerecha(temp);
        }

        if (exito) {
            //    System.out.println("Ruta calculada");
            this.calcularRuta();
            return true;
        } else {
            //    System.out.println("La ruta no pudo calcularse");
            return false;
        }

    }

    private void buscarArriba(Estado e) {
        if (e.y > 0) {
            if (grid[e.x][e.y - 1].isWalkable()) {
                Estado arriba = new Estado(e.x, e.y - 1, Estado.Operator.UP, e);
                if (!historial.contains(arriba)) {
                    colaEstados.add(arriba);
                    historial.add(arriba);
                    if (arriba.equals(objetivo)) {
                        objetivo = arriba;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarAbajo(Estado e) {
        if (e.y < 19) {
            if (grid[e.x][e.y + 1].isWalkable()) {
                Estado abajo = new Estado(e.x, e.y + 1, Estado.Operator.DOWN, e);
                if (!historial.contains(abajo)) {
                    colaEstados.add(abajo);
                    historial.add(abajo);
                    if (abajo.equals(objetivo)) {
                        objetivo = abajo;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarIzquierda(Estado e) {
        if (e.x > 0) {
            if (grid[e.x - 1][e.y].isWalkable()) {
                Estado izquierda = new Estado(e.x - 1, e.y, Estado.Operator.LEFT, e);
                if (!historial.contains(izquierda)) {
                    colaEstados.add(izquierda);
                    historial.add(izquierda);
                    if (izquierda.equals(objetivo)) {
                        objetivo = izquierda;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarDerecha(Estado e) {
        if (e.x < 19) {
            if (grid[e.x + 1][e.y].isWalkable()) {
                Estado derecha = new Estado(e.x + 1, e.y, Estado.Operator.RIGHT, e);
                if (!historial.contains(derecha)) {
                    colaEstados.add(derecha);
                    historial.add(derecha);
                    if (derecha.equals(objetivo)) {
                        objetivo = derecha;
                        exito = true;
                    }
                }
            }
        }
    }

    public void calcularRuta() {
        Estado predecesor = objetivo;
        do {
            pasos.add(0, predecesor.operator);
            predecesor = predecesor.anterior;
        } while (predecesor != null);
        indice_pasos = pasos.size() - 1;

    }

    @Override
    public synchronized void run() {
        destinos.clear();
        destinos.add(new Estado(player.x, player.y, Estado.Operator.NOTHING, null));

        if (!parar) {

            colaEstados.clear();
            historial.clear();
            pasos.clear();

            Estado subinicial, subobjetivo;
            boolean resultado;

            do {

                subinicial = new Estado(thief.x, thief.y, Estado.Operator.NOTHING, null);

                subobjetivo = destinos.get(0);
                resultado = this.buscar(subinicial, subobjetivo);

                if (subinicial.equals(subobjetivo)) {
                    destinos.remove(subobjetivo);
                } else if (!resultado) {
                    colaEstados.clear();
                    historial.clear();
                    pasos.clear();
                    destinos.remove(subobjetivo);
                }

                if (destinos.isEmpty()) {
                    //    System.out.println("Se acabo a donde ir");
                    LETTERS.clear();
                    Platform.runLater(() -> {
                        grid[15][0].setText("PERDISTE!!");
                    });
                    playerTimer.cancel();
                    this.cancel();
                }

            } while (!resultado && !destinos.isEmpty());

            if (pasos.size() > 1) {
                switch (pasos.get(1)) {
                    case DOWN:
                        Platform.runLater(() -> {
                            thief.moveToDown();
                        });
                        break;
                    case UP:
                        Platform.runLater(() -> {
                            thief.moveToUp();
                        });
                        break;
                    case RIGHT:
                        Platform.runLater(() -> {
                            thief.moveToRight();
                        });
                        break;
                    case LEFT:
                        Platform.runLater(() -> {
                            thief.moveToLeft();
                        });
                        break;
                }
            }
        }
    }

}
