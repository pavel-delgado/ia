/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class Letter implements Constantes {

    int portal;
    boolean delivered = false;

    public Letter(int portal) {
        this.portal = portal;
    }

    public void setDelivered(boolean delivered) {
        this.delivered = delivered;
    }

    @Override
    public String toString() {
        String entregada = (delivered) ? "SI" : "NO";
        return "Entregada: " + entregada + " Destino: " + PORTALS.get(portal).x + " " + PORTALS.get(portal).y;
    }

}
