
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class Thief {

    Cell[][] grid;
    int y;
    int x;
    BusquedaRutaThief ia;

    public Thief(Cell[][] grid, int x, int y, Player player, Timer playerTimer) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        grid[x][y].setContent(Cell.CellContent.THIEF);
        ia = new BusquedaRutaThief(grid, this, player, playerTimer);
    }

    public void moveToRight() {
        if (World.checkMarginRight(x)) {
            if (checkMove(grid[x + 1][y])) {
                grid[x + 1][y].setContent(Cell.CellContent.THIEF);
                grid[x][y].clearContent();
                x++;
            }
        }
    }

    public void moveToLeft() {
        if (World.checkMarginLeft(x)) {
            if (checkMove(grid[x - 1][y])) {
                grid[x - 1][y].setContent(Cell.CellContent.THIEF);
                grid[x][y].clearContent();
                x--;
            }
        }
    }

    public void moveToUp() {
        if (World.checkMarginTop(y)) {
            if (checkMove(grid[x][y - 1])) {
                grid[x][y - 1].setContent(Cell.CellContent.THIEF);
                grid[x][y].clearContent();
                y--;
            }
        }
    }

    public void moveToDown() {
        if (World.checkMarginBottom(y)) {
            if (checkMove(grid[x][y + 1])) {
                grid[x][y + 1].setContent(Cell.CellContent.THIEF);
                grid[x][y].clearContent();
                y++;
            }
        }
    }

    public boolean checkMove(Cell cell) {
        return cell.isWalkable();
    }

}
