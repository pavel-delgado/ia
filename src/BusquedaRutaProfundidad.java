
import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public class BusquedaRutaProfundidad implements Runnable {

    Cell[][] grid;
    ArrayList<Estado> colaEstados;
    ArrayList<Estado> historial;
    ArrayList<Estado.Operator> pasos;
    int indice_pasos;
    Estado inicial;
    Estado objetivo;
    Estado temp;
    Player player;
    boolean exito;

    public BusquedaRutaProfundidad(Cell[][] grid, Player player) {
        this.grid = grid;
        colaEstados = new ArrayList<>();
        historial = new ArrayList<>();
        pasos = new ArrayList<>();
        indice_pasos = 0;
        //digo cual es el estado inicial y el final
        inicial = new Estado(0, 2, Estado.Operator.NOTHING, null);
        colaEstados.add(inicial);
        historial.add(inicial);
        objetivo = new Estado(5, 5, Estado.Operator.NOTHING, null);
        exito = false;
        this.player = player;
    }

    public void buscar() {

        if (inicial.equals(objetivo)) {
            exito = true;
        }

        while (!colaEstados.isEmpty() && !exito) {
            temp = colaEstados.get(0);
            colaEstados.remove(0);
            buscarArriba(temp);
            buscarAbajo(temp);
            moverIzquierda(temp);
            moverDerecha(temp);
        }

        if (exito) {
            System.out.println("Ruta calculada");
        } else {
            System.out.println("La ruta no pudo calcularse");
        }

    }

    private void buscarArriba(Estado e) {
        if (e.y > 0) {
            if (grid[e.x][e.y - 1].isWalkable()) {
                Estado arriba = new Estado(e.x, e.y - 1, Estado.Operator.UP, e);
                if (!historial.contains(arriba)) {
                    colaEstados.add(0, arriba);
                    historial.add(arriba);
                    if (arriba.equals(objetivo)) {
                        objetivo = arriba;
                        exito = true;
                    }
                }
            }
        }
    }

    private void buscarAbajo(Estado e) {
        if (e.y < 19) {
            if (grid[e.x][e.y + 1].isWalkable()) {
                Estado abajo = new Estado(e.x, e.y + 1, Estado.Operator.DOWN, e);
                if (!historial.contains(abajo)) {
                    colaEstados.add(0, abajo);
                    historial.add(abajo);
                    if (abajo.equals(objetivo)) {
                        objetivo = abajo;
                        exito = true;
                    }
                }
            }
        }
    }

    private void moverIzquierda(Estado e) {
        if (e.x > 0) {
            if (grid[e.x - 1][e.y].isWalkable()) {
                Estado izquierda = new Estado(e.x - 1, e.y, Estado.Operator.LEFT, e);
                if (!historial.contains(izquierda)) {
                    colaEstados.add(0, izquierda);
                    historial.add(izquierda);
                    if (izquierda.equals(objetivo)) {
                        objetivo = izquierda;
                        exito = true;
                    }
                }
            }
        }
    }

    private void moverDerecha(Estado e) {
        if (e.x < 19) {
            if (grid[e.x + 1][e.y].isWalkable()) {
                Estado derecha = new Estado(e.x + 1, e.y, Estado.Operator.RIGHT, e);
                if (!historial.contains(derecha)) {
                    colaEstados.add(0, derecha);
                    historial.add(derecha);
                    if (derecha.equals(objetivo)) {
                        objetivo = derecha;
                        exito = true;
                    }
                }
            }
        }
    }

    public void calcularRuta() {
        Estado predecesor = objetivo;
        do {
            pasos.add(predecesor.operator);
            predecesor = predecesor.anterior;
        } while (predecesor != null);
        indice_pasos = pasos.size() - 1;

    }

    @Override
    public void run() {
        if (indice_pasos >= 0) {
            switch (pasos.get(indice_pasos)) {
                case DOWN:
                    player.moveToDown();
                    break;
                case UP:
                    player.moveToUp();
                    break;
                case RIGHT:
                    player.moveToRight();
                    break;
                case LEFT:
                    player.moveToLeft();
                    break;
            }
            indice_pasos--;
        } else {
            System.out.println("finalizado");
        }

    }

}
