/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pdelgado
 */
public class Portal {

    int x;
    int y;

    public Portal(Cell[][] grid, int x, int y) {
        this.x = x;
        this.y = y;

        grid[x][y].setContent(Cell.CellContent.META);
    }

}
