
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author pdelgado
 */
public class Bus implements Runnable {

    Cell[][] grid;
    int y;
    int x;
    int p;

    Direction dir = Direction.RIGHT;

    @Override
    public void run() {
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                autoMove();
            }
        };
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(timerTask, 0, 500);
    }

    private void checkStop() {
        if (grid[x][y - 1].getContent() == Cell.CellContent.BUS_STOP) {
            p--;
            System.out.println("Un pasajero ha bajado...");
        }
        if (grid[x][y + 1].getContent() == Cell.CellContent.BUS_STOP) {
            p--;
            System.out.println("Un pasajero ha bajado...");
        }

    }

    public static enum Direction {
        LEFT,
        RIGHT
    }

    public Bus(Cell[][] grid, int x, int y, int p) {
        this.grid = grid;
        this.y = y;
        this.x = x;
        this.p = p;
        grid[x][y].setContent(Cell.CellContent.BUS);

    }

    public void autoMove() {
        Platform.runLater(() -> {
            switch (dir) {
                case RIGHT:
                    if (World.checkMarginRight(x)) {
                        if (checkMove(grid[x + 1][y])) {
//                            if (p > 0) {
//                                checkStop();
//                            }
                            grid[x + 1][y].setContent(Cell.CellContent.BUS);
                            grid[x][y].clearContent();

                            // CLEAR PASSENGERS
                            for (int i = 0; i < grid.length; i++) {
                                if (grid[i][y].getContent() == Cell.CellContent.PASSENGER) {
                                    grid[i][y].clearContent();
                                }
                            }

                            for (int i = x; i > (x - p); i--) {
                                if (i > -1) {
                                    if (grid[i][y].getContent() == Cell.CellContent.NULL) {
                                        grid[i][y].setContent(Cell.CellContent.PASSENGER);
                                    }
                                }
                            }

                            x++;
                        } else {
                            dir = Bus.Direction.values()[(int) ((new Random()).nextDouble() * 2)];
                        }
                    } else {
                        dir = Bus.Direction.values()[(int) ((new Random()).nextDouble() * 2)];
                    }
                    break;
                case LEFT:
                    if (World.checkMarginLeft(x)) {
                        if (checkMove(grid[x - 1][y])) {
                            if (p > 0) {
                                checkStop();
                            }

                            grid[x - 1][y].setContent(Cell.CellContent.BUS);
                            grid[x][y].clearContent();

                            // CLEAR PASSENGERS
                            for (int i = 0; i < grid.length; i++) {
                                if (grid[i][y].getContent() == Cell.CellContent.PASSENGER) {
                                    grid[i][y].clearContent();
                                }
                            }

                            for (int i = x; i < (x + p); i++) {
                                if (i < 20) {
                                    if (grid[i][y].getContent() == Cell.CellContent.NULL) {
                                        grid[i][y].setContent(Cell.CellContent.PASSENGER);
                                    }
                                }
                            }
                            x--;
                        } else {
                            dir = Bus.Direction.values()[(int) ((new Random()).nextDouble() * 2)];
                        }
                    } else {
                        dir = Bus.Direction.values()[(int) ((new Random()).nextDouble() * 2)];
                    }
                    break;

            }

        });

    }

    public boolean checkMove(Cell cell) {
        boolean ground = false;
        boolean content = false;

        switch (cell.ground) {
            case WAY_H:
                ground = true;
                break;
            case WAY_V:
                ground = true;
                break;
            case ZEBRA_V:
                ground = true;
                break;
        }

        switch (cell.content) {
            case NULL:
                content = true;
                break;
            case PASSENGER:
                content = true;
                break;
        }

        return ground && content;

    }

}
