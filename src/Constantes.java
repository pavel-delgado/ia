
import java.util.ArrayList;
import java.util.Queue;
import java.util.Stack;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author NICK
 */
public interface Constantes {

    int WIDTH = 30;
    int HEIGHT = 20;
    int N_PASAJEROS = 10;
    int DEEP = 32;
    int TEXT_X = 20;
    int TEXT_Y = 3;
    boolean SEGURO = true;

    ArrayList<Letter> LETTERS = new ArrayList<>();
    ArrayList<Portal> PORTALS = new ArrayList<>();

    int T = 3; // TRAFICO
    int P = 3; // POBLACION

}
